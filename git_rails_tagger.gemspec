# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'git_rails_tagger/version'

Gem::Specification.new do |spec|
  spec.name          = 'git_rails_tagger'
  spec.version       = GitRailsTagger::VERSION
  spec.authors       = ['Adam Stasio']
  spec.email         = ['adams@scimedsolutions.com']
  spec.description   = 'Adds a rake task which allows you to easily tag your project in Git.'
  spec.summary       = 'Adds a rake task which allows you to easily tag your project in Git.'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 3.1'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
end
