require 'git_rails_tagger/version'
require 'git_rails_tagger/app_version'
require 'git_rails_tagger/railtie' if defined?(Rails)

module GitRailsTagger
  require 'git_rails_tagger/engine' if defined?(Rails)

  def self.root
    if defined?(Rails)
      Rails.root.to_s
    else
      Dir.pwd
    end
  end
end
