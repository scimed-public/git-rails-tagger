require 'yaml'

module GitRailsTagger
  class AppVersion
    attr_accessor :major, :minor, :revision, :release_candidate, :is_release_candidate

    def initialize(attrs = {})
      if attrs[:version]
        (self.major, self.minor, self.revision, self.release_candidate) = AppVersion.parse_version_string(attrs[:version])
        self.is_release_candidate = true if !self.release_candidate.nil? && self.release_candidate != ''
      else
        self.major = attrs[:major].to_i || 1
        self.minor = attrs[:minor].to_i || 0
        self.revision = attrs[:revision].to_i || 0
        if !attrs[:release_candidate].nil? && attrs[:release_candidate] != ''
          self.is_release_candidate = true
          self.release_candidate = attrs[:release_candidate].to_i
        else
          self.is_release_candidate = false
          self.release_candidate = nil
        end
      end
    end

    def release_candidate?
      self.is_release_candidate
    end

    def to_s
      str = "#{major}.#{minor}.#{revision}"
      str += ".rc-#{release_candidate}" if release_candidate?
      str
    end

    def self.parse_version_string(version_str)
      (major, minor, revision, release_candidate) = version_str.split(/_|\./)
      release_candidate_number = nil
      if release_candidate
        release_candidate_number = release_candidate.split('-')[1]
        release_candidate_number = release_candidate_number.to_i
      end
      [major.to_i, minor.to_i, revision.to_i, release_candidate_number]
    end

    def bump_major
      self.major += 1
      self.minor = 0
      self.revision = 0
      self.release_candidate = nil
      self.is_release_candidate = false
    end

    def bump_minor
      self.minor += 1
      self.revision = 0
      self.release_candidate = nil
      self.is_release_candidate = false
    end

    def bump_revision
      self.revision += 1
      self.release_candidate = nil
      self.is_release_candidate = false
    end

    # Note: if this is not a release candidate version, it first bumps the minor version.
    def bump_release_candidate
      if release_candidate?
        self.release_candidate += 1
      else
        bump_minor
        self.release_candidate = 0
      end
      self.is_release_candidate = true
    end

    def self.current_version
      yml = YAML.load_file(
        "#{GitRailsTagger.root}/config/app_version.yml",
        permitted_classes: [Time]
      )
      app_version = GitRailsTagger::AppVersion.new(
        :major             => yml['major'],
        :minor             => yml['minor'],
        :revision          => yml['revision'],
        :release_candidate => yml['release_candidate']
      )
      app_version
    end
  end
end
