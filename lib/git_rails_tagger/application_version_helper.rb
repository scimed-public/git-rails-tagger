module GitRailsTagger
  module ApplicationVersionHelper
    def self.application_version
      current_version = GitRailsTagger::AppVersion.current_version
      begin
        str = "#{current_version.major}.#{current_version.minor}.#{current_version.revision}"
        str += ".rc-#{current_version.release_candidate}" if current_version.release_candidate
        str
      rescue StandardError
        '--'
      end
    end
  end
end
