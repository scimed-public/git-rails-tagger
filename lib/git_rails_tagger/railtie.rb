require 'git_rails_tagger/application_version_helper'

module GitRailsTagger
  class Railtie < Rails::Railtie
    initializer 'git_rails_tagger.application_version_helper' do
      ActionView::Base.send :include, ApplicationVersionHelper
    end
  end
end
