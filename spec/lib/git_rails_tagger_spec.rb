require 'spec_helper'

describe GitRailsTagger::AppVersion do
  it 'increments the revision version' do
    version = GitRailsTagger::AppVersion.new(:version => '3.5.8')
    version.bump_revision
    version.to_s.should == '3.5.9'
  end

  it 'increments the minor version' do
    version = GitRailsTagger::AppVersion.new(:version => '3.5.8')
    version.bump_minor
    version.to_s.should == '3.6.0'
  end

  it 'increments the major version' do
    version = GitRailsTagger::AppVersion.new(:version => '3.5.8')
    version.bump_major
    version.to_s.should == '4.0.0'
  end

  it 'increments the release-candidate' do
    version = GitRailsTagger::AppVersion.new(:version => '4.3.5.rc-6')
    version.bump_release_candidate
    version.to_s.should == '4.3.5.rc-7'
  end

  it 'is able to be initialized from individual numbers' do
    version = GitRailsTagger::AppVersion.new(:major => 4, :minor => 3, :revision => 5, :release_candidate => 6)
    version.major.should == 4
    version.to_s.should == '4.3.5.rc-6'
  end
end
