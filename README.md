# GitRailsTagger

This gem adds a rake task to let you easily tag your project in Git. It can automatically bump the version number for you. It supports release candidate numbering. It creates a yml file in your project to document what version of the application you have deployed.

## Installation

Add this line to your application's Gemfile:

    gem 'git_rails_tagger', git: 'git@git.scimedsolutions.com:scimed/git_rails_tagger.git'

And then execute:

    $ bundle

Then use it by running:

    $ rake git:tag:bump_release_candidate

Or install it yourself as:

    $ gem install git_rails_tagger

## Example Usage

    rake git:tag:bump_major # Changes the major version (e.g. 1.2.3 to 2.0.0)
    rake git:tag:bump_minor # Changes the minor version (e.g. 1.2.3 to 1.3.0)
    rake git:tag:bump_revision # Changes the revision (e.g. 1.2.3 to 1.2.4)
    rake git:tag:bump_release_candidate # Changes the release candidate (e.g. 1.2.3.rc-0 to 1.2.4.rc-1)
    rake git:tag TAG=2.1.0 # Changes the version to the specified value (e.g. 1.2.3 to 2.1.0)

Note that `bump_release_candidate` is also aliased to `bump`.

## Developing

If you want to run the gem without installing it, execute:

    $ bundle console

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Possible Todo's

* Add to gem server
* Add a helper for application_version
* Test & tweak
